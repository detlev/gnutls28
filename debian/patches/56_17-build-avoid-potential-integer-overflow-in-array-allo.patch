From 584783a3fc2048ca6673ccdc4f44d0ffc46080cb Mon Sep 17 00:00:00 2001
From: Daiki Ueno <ueno@gnu.org>
Date: Sun, 21 Feb 2021 08:43:26 +0100
Subject: [PATCH 3/5] build: avoid potential integer overflow in array
 allocation

This relies on _gnutls_reallocarray for all occasions of array
allocations, so that they can benefit from the built-in overflow
checks.

Signed-off-by: Daiki Ueno <ueno@gnu.org>
---
 lib/cert-cred-x509.c    | 23 ++++++++++++++--------
 lib/cert-cred.c         | 18 +++++++++--------
 lib/hello_ext.c         |  4 +++-
 lib/pcert.c             |  3 ++-
 lib/pkcs11.c            | 11 +++++++----
 lib/pkcs11x.c           |  5 ++++-
 lib/supplemental.c      |  4 ++--
 lib/x509/crl.c          |  7 +++----
 lib/x509/ocsp.c         | 11 ++++++-----
 lib/x509/pkcs12.c       | 15 ++++++--------
 lib/x509/verify-high.c  | 43 +++++++++++++++++------------------------
 lib/x509/verify-high2.c |  6 ++++--
 lib/x509/x509.c         |  9 ++++-----
 lib/x509/x509_ext.c     | 12 ++++++------
 14 files changed, 90 insertions(+), 81 deletions(-)

diff --git a/lib/cert-cred-x509.c b/lib/cert-cred-x509.c
index 1b44f3a634..543a1155f4 100644
--- a/lib/cert-cred-x509.c
+++ b/lib/cert-cred-x509.c
@@ -254,7 +254,7 @@ parse_pem_cert_mem(gnutls_certificate_credentials_t res,
 		goto cleanup;
 	}
 
-	pcerts = gnutls_malloc(sizeof(gnutls_pcert_st) * count);
+	pcerts = _gnutls_reallocarray(NULL, count, sizeof(gnutls_pcert_st));
 	if (pcerts == NULL) {
 		gnutls_assert();
 		return GNUTLS_E_MEMORY_ERROR;
@@ -441,7 +441,8 @@ read_cert_url(gnutls_certificate_credentials_t res, gnutls_privkey_t key, const
 
 	_gnutls_str_array_init(&names);
 
-	ccert = gnutls_malloc(sizeof(*ccert)*MAX_PKCS11_CERT_CHAIN);
+	ccert = _gnutls_reallocarray(NULL, MAX_PKCS11_CERT_CHAIN,
+				     sizeof(*ccert));
 	if (ccert == NULL) {
 		gnutls_assert();
 		ret = GNUTLS_E_MEMORY_ERROR;
@@ -770,7 +771,8 @@ gnutls_certificate_set_x509_key(gnutls_certificate_credentials_t res,
 	}
 
 	/* load certificates */
-	pcerts = gnutls_malloc(sizeof(gnutls_pcert_st) * cert_list_size);
+	pcerts = _gnutls_reallocarray(NULL, cert_list_size,
+				      sizeof(gnutls_pcert_st));
 	if (pcerts == NULL) {
 		gnutls_assert();
 		return GNUTLS_E_MEMORY_ERROR;
@@ -901,8 +903,9 @@ gnutls_certificate_get_x509_crt(gnutls_certificate_credentials_t res,
 	}
 
 	*crt_list_size = res->certs[index].cert_list_length;
-	*crt_list = gnutls_malloc(
-		res->certs[index].cert_list_length * sizeof (gnutls_x509_crt_t));
+	*crt_list = _gnutls_reallocarray(NULL,
+					 res->certs[index].cert_list_length,
+					 sizeof (gnutls_x509_crt_t));
 	if (*crt_list == NULL) {
 		gnutls_assert();
 		return GNUTLS_E_MEMORY_ERROR;
@@ -1151,7 +1154,8 @@ gnutls_certificate_set_x509_trust(gnutls_certificate_credentials_t res,
 	if (ca_list == NULL || ca_list_size < 1)
 		return gnutls_assert_val(GNUTLS_E_INVALID_REQUEST);
 
-	new_list = gnutls_malloc(ca_list_size * sizeof(gnutls_x509_crt_t));
+	new_list = _gnutls_reallocarray(NULL, ca_list_size,
+					sizeof(gnutls_x509_crt_t));
 	if (!new_list)
 		return GNUTLS_E_MEMORY_ERROR;
 
@@ -1335,12 +1339,15 @@ gnutls_certificate_set_x509_crl(gnutls_certificate_credentials_t res,
 				int crl_list_size)
 {
 	int ret, i, j;
-	gnutls_x509_crl_t *new_crl = gnutls_malloc(crl_list_size * sizeof(gnutls_x509_crl_t));
-	unsigned flags = GNUTLS_TL_USE_IN_TLS;
+	gnutls_x509_crl_t *new_crl;
+	unsigned flags;
 
+	flags = GNUTLS_TL_USE_IN_TLS;
 	if (res->flags & GNUTLS_CERTIFICATE_VERIFY_CRLS)
 		flags |= GNUTLS_TL_VERIFY_CRL|GNUTLS_TL_FAIL_ON_INVALID_CRL;
 
+	new_crl = _gnutls_reallocarray(NULL, crl_list_size,
+				       sizeof(gnutls_x509_crl_t));
 	if (!new_crl)
 		return GNUTLS_E_MEMORY_ERROR;
 
diff --git a/lib/cert-cred.c b/lib/cert-cred.c
index 28b67f60df..3f1041f73b 100644
--- a/lib/cert-cred.c
+++ b/lib/cert-cred.c
@@ -55,15 +55,15 @@ _gnutls_certificate_credential_append_keypair(gnutls_certificate_credentials_t r
 				       gnutls_pcert_st * crt,
 				       int nr)
 {
-	res->sorted_cert_idx = gnutls_realloc_fast(res->sorted_cert_idx,
-						(1 + res->ncerts) *
-						sizeof(unsigned int));
+	res->sorted_cert_idx = _gnutls_reallocarray_fast(res->sorted_cert_idx,
+							 res->ncerts + 1,
+							 sizeof(unsigned int));
 	if (res->sorted_cert_idx == NULL)
 		return gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 
-	res->certs = gnutls_realloc_fast(res->certs,
-					 (1 + res->ncerts) *
-					 sizeof(certs_st));
+	res->certs = _gnutls_reallocarray_fast(res->certs,
+					       res->ncerts + 1,
+					       sizeof(certs_st));
 	if (res->certs == NULL)
 		return gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 
@@ -204,7 +204,8 @@ gnutls_certificate_set_key(gnutls_certificate_credentials_t res,
 		gnutls_privkey_set_pin_function(key, res->pin.cb,
 						res->pin.data);
 
-	new_pcert_list = gnutls_malloc(sizeof(gnutls_pcert_st) * pcert_list_size);
+	new_pcert_list = _gnutls_reallocarray(NULL, pcert_list_size,
+					      sizeof(gnutls_pcert_st));
 	if (new_pcert_list == NULL) {
 		return gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 	}
@@ -451,7 +452,8 @@ static gnutls_pcert_st *alloc_and_load_x509_certs(gnutls_x509_crt_t *
 	if (certs == NULL)
 		return NULL;
 
-	local_certs = gnutls_malloc(sizeof(gnutls_pcert_st) * ncerts);
+	local_certs = _gnutls_reallocarray(NULL, ncerts,
+					   sizeof(gnutls_pcert_st));
 	if (local_certs == NULL) {
 		gnutls_assert();
 		return NULL;
diff --git a/lib/hello_ext.c b/lib/hello_ext.c
index dbe73fdeb3..6943d095b3 100644
--- a/lib/hello_ext.c
+++ b/lib/hello_ext.c
@@ -923,7 +923,9 @@ gnutls_session_ext_register(gnutls_session_t session,
 			tmp_mod.validity |= GNUTLS_EXT_FLAG_TLS;
 	}
 
-	exts = gnutls_realloc(session->internals.rexts, (session->internals.rexts_size+1)*sizeof(*exts));
+	exts = _gnutls_reallocarray(session->internals.rexts,
+				    session->internals.rexts_size + 1,
+				    sizeof(*exts));
 	if (exts == NULL) {
 		return gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 	}
diff --git a/lib/pcert.c b/lib/pcert.c
index 89d3d40e63..e5057aec51 100644
--- a/lib/pcert.c
+++ b/lib/pcert.c
@@ -185,7 +185,8 @@ gnutls_pcert_list_import_x509_raw(gnutls_pcert_st *pcert_list,
 	unsigned int i = 0, j;
 	gnutls_x509_crt_t *crt;
 
-	crt = gnutls_malloc((*pcert_list_size) * sizeof(gnutls_x509_crt_t));
+	crt = _gnutls_reallocarray(NULL, *pcert_list_size,
+				   sizeof(gnutls_x509_crt_t));
 
 	if (crt == NULL)
 		return gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
diff --git a/lib/pkcs11.c b/lib/pkcs11.c
index 0d5e83a0c6..7e8c0570ac 100644
--- a/lib/pkcs11.c
+++ b/lib/pkcs11.c
@@ -3059,8 +3059,8 @@ find_privkeys(struct pkcs11_session_info *sinfo,
 		return GNUTLS_E_REQUESTED_DATA_NOT_AVAILABLE;
 	}
 
-	list->key_ids =
-	    gnutls_malloc(sizeof(gnutls_buffer_st) * list->key_ids_size);
+	list->key_ids = _gnutls_reallocarray(NULL, list->key_ids_size,
+					     sizeof(gnutls_buffer_st));
 	if (list->key_ids == NULL) {
 		gnutls_assert();
 		return GNUTLS_E_MEMORY_ERROR;
@@ -3277,7 +3277,7 @@ find_multi_objs_cb(struct ck_function_list *module, struct pkcs11_session_info *
 		return pkcs11_rv_to_err(rv);
 	}
 
-	ctx = gnutls_malloc(OBJECTS_A_TIME*sizeof(ctx[0]));
+	ctx = _gnutls_reallocarray(NULL, OBJECTS_A_TIME, sizeof(ctx[0]));
 	if (ctx == NULL) {
 		ret = gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 		goto fail;
@@ -3291,7 +3291,10 @@ find_multi_objs_cb(struct ck_function_list *module, struct pkcs11_session_info *
 		unsigned j;
 		gnutls_datum_t id;
 
-		find_data->p_list = gnutls_realloc_fast(find_data->p_list, (find_data->current+count)*sizeof(find_data->p_list[0]));
+		find_data->p_list =
+			_gnutls_reallocarray_fast(find_data->p_list,
+						  find_data->current + count,
+						  sizeof(find_data->p_list[0]));
 		if (find_data->p_list == NULL) {
 			ret = gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 			goto fail;
diff --git a/lib/pkcs11x.c b/lib/pkcs11x.c
index ee0b431685..7bb62f64bf 100644
--- a/lib/pkcs11x.c
+++ b/lib/pkcs11x.c
@@ -217,7 +217,10 @@ find_ext_cb(struct ck_function_list *module, struct pkcs11_session_info *sinfo,
 		rv = pkcs11_get_attribute_avalue(sinfo->module, sinfo->pks, obj, CKA_VALUE, &ext);
 		if (rv == CKR_OK) {
 
-			find_data->exts = gnutls_realloc_fast(find_data->exts, (1+find_data->exts_size)*sizeof(find_data->exts[0]));
+			find_data->exts =
+				_gnutls_reallocarray_fast(find_data->exts,
+							  find_data->exts_size + 1,
+							  sizeof(find_data->exts[0]));
 			if (find_data->exts == NULL) {
 				ret = gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 				goto cleanup;
diff --git a/lib/supplemental.c b/lib/supplemental.c
index 07b38cc938..fc9545526a 100644
--- a/lib/supplemental.c
+++ b/lib/supplemental.c
@@ -252,8 +252,8 @@ _gnutls_supplemental_register(gnutls_supplemental_entry_st *entry)
 			return gnutls_assert_val(GNUTLS_E_ALREADY_REGISTERED);
 	}
 
-	p = gnutls_realloc_fast(suppfunc,
-				sizeof(*suppfunc) * (suppfunc_size + 1));
+	p = _gnutls_reallocarray_fast(suppfunc, suppfunc_size + 1,
+				      sizeof(*suppfunc));
 	if (!p) {
 		gnutls_assert();
 		return GNUTLS_E_MEMORY_ERROR;
diff --git a/lib/x509/crl.c b/lib/x509/crl.c
index 8705be3b6c..aea8bc6d33 100644
--- a/lib/x509/crl.c
+++ b/lib/x509/crl.c
@@ -1262,7 +1262,7 @@ gnutls_x509_crl_list_import2(gnutls_x509_crl_t ** crls,
 	unsigned int init = 1024;
 	int ret;
 
-	*crls = gnutls_malloc(sizeof(gnutls_x509_crl_t) * init);
+	*crls = _gnutls_reallocarray(NULL, init, sizeof(gnutls_x509_crl_t));
 	if (*crls == NULL) {
 		gnutls_assert();
 		return GNUTLS_E_MEMORY_ERROR;
@@ -1272,9 +1272,8 @@ gnutls_x509_crl_list_import2(gnutls_x509_crl_t ** crls,
 	    gnutls_x509_crl_list_import(*crls, &init, data, format,
 					flags | GNUTLS_X509_CRT_LIST_IMPORT_FAIL_IF_EXCEED);
 	if (ret == GNUTLS_E_SHORT_MEMORY_BUFFER) {
-		*crls =
-		    gnutls_realloc_fast(*crls,
-					sizeof(gnutls_x509_crl_t) * init);
+		*crls = _gnutls_reallocarray_fast(*crls, init,
+						  sizeof(gnutls_x509_crl_t));
 		if (*crls == NULL) {
 			gnutls_assert();
 			return GNUTLS_E_MEMORY_ERROR;
diff --git a/lib/x509/ocsp.c b/lib/x509/ocsp.c
index 0c71a6e462..7587a2649a 100644
--- a/lib/x509/ocsp.c
+++ b/lib/x509/ocsp.c
@@ -1905,9 +1905,8 @@ gnutls_ocsp_resp_get_certs(gnutls_ocsp_resp_const_t resp,
 			goto error;
 		}
 
-		tmpcerts2 =
-		    gnutls_realloc_fast(tmpcerts,
-					(ctr + 2) * sizeof(*tmpcerts));
+		tmpcerts2 = _gnutls_reallocarray_fast(tmpcerts, ctr + 2,
+						      sizeof(*tmpcerts));
 		if (tmpcerts2 == NULL) {
 			gnutls_assert();
 			ret = GNUTLS_E_MEMORY_ERROR;
@@ -2458,7 +2457,9 @@ gnutls_ocsp_resp_list_import2(gnutls_ocsp_resp_t **ocsps,
 				goto fail;
 			}
 
-			new_ocsps = gnutls_realloc(*ocsps, (*size + 1)*sizeof(gnutls_ocsp_resp_t));
+			new_ocsps = _gnutls_reallocarray(*ocsps,
+							 *size + 1,
+							 sizeof(gnutls_ocsp_resp_t));
 			if (new_ocsps == NULL) {
 				resp = NULL;
 				gnutls_assert();
@@ -2492,7 +2493,7 @@ gnutls_ocsp_resp_list_import2(gnutls_ocsp_resp_t **ocsps,
 			goto cleanup;
 		}
 
-		*ocsps = gnutls_malloc(1*sizeof(gnutls_ocsp_resp_t));
+		*ocsps = gnutls_malloc(sizeof(gnutls_ocsp_resp_t));
 		if (*ocsps == NULL) {
 			gnutls_assert();
 			ret = GNUTLS_E_MEMORY_ERROR;
diff --git a/lib/x509/pkcs12.c b/lib/x509/pkcs12.c
index 2dc0823905..ac0d2f565f 100644
--- a/lib/x509/pkcs12.c
+++ b/lib/x509/pkcs12.c
@@ -1455,10 +1455,9 @@ static int make_chain(gnutls_x509_crt_t ** chain, unsigned int *chain_len,
 			    != 0)
 				goto skip;
 
-			*chain =
-			    gnutls_realloc_fast(*chain,
-						sizeof((*chain)[0]) *
-						++(*chain_len));
+			*chain = _gnutls_reallocarray_fast(*chain,
+							   ++(*chain_len),
+							   sizeof((*chain)[0]));
 			if (*chain == NULL) {
 				gnutls_assert();
 				return GNUTLS_E_MEMORY_ERROR;
@@ -1779,11 +1778,9 @@ gnutls_pkcs12_simple_parse(gnutls_pkcs12_t p12,
 
 				if (memcmp(cert_id, key_id, cert_id_size) != 0) {	/* they don't match - skip the certificate */
 					_extra_certs =
-						gnutls_realloc_fast
-						(_extra_certs,
-						 sizeof(_extra_certs
-							[0]) *
-						 ++_extra_certs_len);
+						_gnutls_reallocarray_fast(_extra_certs,
+									  ++_extra_certs_len,
+									  sizeof(_extra_certs[0]));
 					if (!_extra_certs) {
 						gnutls_assert();
 						ret =
diff --git a/lib/x509/verify-high.c b/lib/x509/verify-high.c
index 736326ee18..7fdbdc68d5 100644
--- a/lib/x509/verify-high.c
+++ b/lib/x509/verify-high.c
@@ -129,9 +129,9 @@ cert_set_add(struct cert_set_st *set, const gnutls_x509_crt_t cert)
 	hash %= set->size;
 
 	set->node[hash].certs =
-		gnutls_realloc_fast(set->node[hash].certs,
-				    (set->node[hash].size + 1) *
-				    sizeof(*set->node[hash].certs));
+		_gnutls_reallocarray_fast(set->node[hash].certs,
+					  set->node[hash].size + 1,
+					  sizeof(*set->node[hash].certs));
 	if (!set->node[hash].certs) {
 		return gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 	}
@@ -298,10 +298,9 @@ trust_list_add_compat(gnutls_x509_trust_list_t list,
 			       gnutls_x509_crt_t cert)
 {
 	list->keep_certs =
-		    gnutls_realloc_fast(list->keep_certs,
-					(list->keep_certs_size +
-					 1) *
-					sizeof(list->keep_certs[0]));
+		_gnutls_reallocarray_fast(list->keep_certs,
+					  list->keep_certs_size + 1,
+					  sizeof(list->keep_certs[0]));
 	if (list->keep_certs == NULL) {
 		gnutls_assert();
 		return GNUTLS_E_MEMORY_ERROR;
@@ -380,11 +379,9 @@ gnutls_x509_trust_list_add_cas(gnutls_x509_trust_list_t list,
 		}
 
 		list->node[hash].trusted_cas =
-		    gnutls_realloc_fast(list->node[hash].trusted_cas,
-					(list->node[hash].trusted_ca_size +
-					 1) *
-					sizeof(list->node[hash].
-					       trusted_cas[0]));
+			_gnutls_reallocarray_fast(list->node[hash].trusted_cas,
+						  list->node[hash].trusted_ca_size + 1,
+						  sizeof(list->node[hash].trusted_cas[0]));
 		if (list->node[hash].trusted_cas == NULL) {
 			gnutls_assert();
 			return i;
@@ -671,9 +668,9 @@ gnutls_x509_trust_list_remove_cas(gnutls_x509_trust_list_t list,
 		 * ensure that a server certificate will also get rejected.
 		 */
 		list->blacklisted =
-		    gnutls_realloc_fast(list->blacklisted,
-				(list->blacklisted_size + 1) *
-				sizeof(list->blacklisted[0]));
+			_gnutls_reallocarray_fast(list->blacklisted,
+						  list->blacklisted_size + 1,
+						  sizeof(list->blacklisted[0]));
 		if (list->blacklisted == NULL)
 			return gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 
@@ -729,10 +726,9 @@ gnutls_x509_trust_list_add_named_crt(gnutls_x509_trust_list_t list,
 	hash %= list->size;
 
 	list->node[hash].named_certs =
-	    gnutls_realloc_fast(list->node[hash].named_certs,
-				(list->node[hash].named_cert_size +
-				 1) *
-				sizeof(list->node[hash].named_certs[0]));
+		_gnutls_reallocarray_fast(list->node[hash].named_certs,
+					  list->node[hash].named_cert_size + 1,
+					  sizeof(list->node[hash].named_certs[0]));
 	if (list->node[hash].named_certs == NULL)
 		return gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 
@@ -842,12 +838,9 @@ gnutls_x509_trust_list_add_crls(gnutls_x509_trust_list_t list,
 			}
 		}
 
-		tmp =
-		    gnutls_realloc(list->node[hash].crls,
-					(list->node[hash].crl_size +
-					 1) *
-					sizeof(list->node[hash].
-					       crls[0]));
+		tmp = _gnutls_reallocarray(list->node[hash].crls,
+					   list->node[hash].crl_size + 1,
+					   sizeof(list->node[hash].crls[0]));
 		if (tmp == NULL) {
 			ret = i;
 			gnutls_assert();
diff --git a/lib/x509/verify-high2.c b/lib/x509/verify-high2.c
index 9820595e97..16d757cf8a 100644
--- a/lib/x509/verify-high2.c
+++ b/lib/x509/verify-high2.c
@@ -216,7 +216,8 @@ int add_trust_list_pkcs11_object_url(gnutls_x509_trust_list_t list, const char *
 		goto cleanup;
 	}
 
-	xcrt_list = gnutls_malloc(sizeof(gnutls_x509_crt_t) * pcrt_list_size);
+	xcrt_list = _gnutls_reallocarray(NULL, pcrt_list_size,
+					 sizeof(gnutls_x509_crt_t));
 	if (xcrt_list == NULL) {
 		ret = GNUTLS_E_MEMORY_ERROR;
 		goto cleanup;
@@ -264,7 +265,8 @@ int remove_pkcs11_object_url(gnutls_x509_trust_list_t list, const char *url)
 		goto cleanup;
 	}
 
-	xcrt_list = gnutls_malloc(sizeof(gnutls_x509_crt_t) * pcrt_list_size);
+	xcrt_list = _gnutls_reallocarray(NULL, pcrt_list_size,
+					 sizeof(gnutls_x509_crt_t));
 	if (xcrt_list == NULL) {
 		ret = GNUTLS_E_MEMORY_ERROR;
 		goto cleanup;
diff --git a/lib/x509/x509.c b/lib/x509/x509.c
index c3801a83f2..4e494d10e0 100644
--- a/lib/x509/x509.c
+++ b/lib/x509/x509.c
@@ -3700,7 +3700,7 @@ gnutls_x509_crt_list_import2(gnutls_x509_crt_t ** certs,
 	unsigned int init = 1024;
 	int ret;
 
-	*certs = gnutls_malloc(sizeof(gnutls_x509_crt_t) * init);
+	*certs = _gnutls_reallocarray(NULL, init, sizeof(gnutls_x509_crt_t));
 	if (*certs == NULL) {
 		gnutls_assert();
 		return GNUTLS_E_MEMORY_ERROR;
@@ -3710,9 +3710,8 @@ gnutls_x509_crt_list_import2(gnutls_x509_crt_t ** certs,
 	    gnutls_x509_crt_list_import(*certs, &init, data, format,
 					flags | GNUTLS_X509_CRT_LIST_IMPORT_FAIL_IF_EXCEED);
 	if (ret == GNUTLS_E_SHORT_MEMORY_BUFFER) {
-		*certs =
-		    gnutls_realloc_fast(*certs,
-					sizeof(gnutls_x509_crt_t) * init);
+		*certs = _gnutls_reallocarray_fast(*certs, init,
+						   sizeof(gnutls_x509_crt_t));
 		if (*certs == NULL) {
 			gnutls_assert();
 			return GNUTLS_E_MEMORY_ERROR;
@@ -4375,7 +4374,7 @@ gnutls_x509_crt_list_import_url(gnutls_x509_crt_t **certs,
 		gnutls_free(issuer.data);
 	}
 
-	*certs = gnutls_malloc(total*sizeof(gnutls_x509_crt_t));
+	*certs = _gnutls_reallocarray(NULL, total, sizeof(gnutls_x509_crt_t));
 	if (*certs == NULL) {
 		ret = GNUTLS_E_MEMORY_ERROR;
 		goto cleanup;
diff --git a/lib/x509/x509_ext.c b/lib/x509/x509_ext.c
index 6aeb159dba..fda936e8ff 100644
--- a/lib/x509/x509_ext.c
+++ b/lib/x509/x509_ext.c
@@ -137,7 +137,7 @@ int subject_alt_names_set(struct name_st **names,
 	void *tmp;
 	int ret;
 
-	tmp = gnutls_realloc(*names, (*size + 1) * sizeof((*names)[0]));
+	tmp = _gnutls_reallocarray(*names, *size + 1, sizeof((*names)[0]));
 	if (tmp == NULL) {
 		return gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 	}
@@ -2317,9 +2317,8 @@ int crl_dist_points_set(gnutls_x509_crl_dist_points_t cdp,
 	void *tmp;
 
 	/* new dist point */
-	tmp =
-	    gnutls_realloc(cdp->points,
-			   (cdp->size + 1) * sizeof(cdp->points[0]));
+	tmp = _gnutls_reallocarray(cdp->points, cdp->size + 1,
+				   sizeof(cdp->points[0]));
 	if (tmp == NULL) {
 		return gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 	}
@@ -2734,7 +2733,7 @@ int gnutls_x509_aia_set(gnutls_x509_aia_t aia,
 	void *tmp;
 	unsigned indx;
 
-	tmp = gnutls_realloc(aia->aia, (aia->size + 1) * sizeof(aia->aia[0]));
+	tmp = _gnutls_reallocarray(aia->aia, aia->size + 1, sizeof(aia->aia[0]));
 	if (tmp == NULL) {
 		return gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 	}
@@ -2786,7 +2785,8 @@ static int parse_aia(ASN1_TYPE c2, gnutls_x509_aia_t aia)
 		}
 
 		indx = aia->size;
-		tmp = gnutls_realloc(aia->aia, (aia->size + 1) * sizeof(aia->aia[0]));
+		tmp = _gnutls_reallocarray(aia->aia, aia->size + 1,
+					   sizeof(aia->aia[0]));
 		if (tmp == NULL) {
 			return gnutls_assert_val(GNUTLS_E_MEMORY_ERROR);
 		}
-- 
2.30.2

